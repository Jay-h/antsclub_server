package cn.tsingyu.antsclub.model;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@SuppressWarnings("serial")
public class Topic extends Model<Topic>{
	public static final Topic dao = new Topic();
	public Page<Topic> topicList(int pageNumber, int pageSize,String cid) {
		return paginate(pageNumber, pageSize, "select id,title ", " from topic  where cid=? order by id desc",cid);
	}
}
