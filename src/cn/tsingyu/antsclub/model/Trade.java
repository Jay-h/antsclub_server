package cn.tsingyu.antsclub.model;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@SuppressWarnings("serial")
public class Trade extends Model<Trade>{
	public static final Trade dao = new Trade();
	public Trade getTrade(String actId,String userId){
		return findFirst("select * from trade where activityId=? and sourceId=? order by id desc",actId,userId);
	}
	public Page<Trade> myTrade(int pageNumber, int pageSize,String userId) {
		return paginate(pageNumber, pageSize, "select  substring(createTime,1,16) as createTime,tradeAmount,ifnull(activityName,'') as activityName,remark", " from trade  where sourceId=? order by id desc",userId);
	}
}
