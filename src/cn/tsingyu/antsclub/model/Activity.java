package cn.tsingyu.antsclub.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

@SuppressWarnings("serial")
public class Activity extends Model<Activity>{
	public static final Activity dao = new Activity();
	public Page<Activity> paginate(int pageNumber, int pageSize) {
		return paginate(pageNumber, pageSize, "select * ", "from activity order by id desc");
	}
	public Page<Activity> myList(int pageNumber, int pageSize,String uid){
		return paginate(pageNumber, pageSize, "select a.*,ua.signNum,ua.signName "," from activity a left join user_activity ua on a.id=ua.userId where a.id = ? ",uid);
	}
	public Record getFunllInfo(String actId){
		List<Record> list=Db.find("SELECT a.id as aid,typeName,payTypeCode,payTypeName,payAmount,title,startTime,endTime,address,detail,maxNum,"
						+" createUserName,userId,signName,signNum FROM user_activity ua "
						+" right join activity a  on a.id = ua.activityId "
						+" where a.id=? order by signTime asc ",actId);
		return composeSignInfo(list);
	}
	public Record getSignInfo(String actId){
		List<Record> list=Db.find("SELECT userId,signName,signNum FROM user_activity "
						+" where activityId=? order by signTime asc ",actId);
		return composeSignInfo(list);
	}
	public Record composeSignInfo(List<Record> list){
		Record a = new Record();
		String signIds= "";
		String signNames = "";
		int signNum=0;
		if(list.size()>0){
			a = list.get(0);
			for (Record record : list) {
				signIds+="，"+record.getStr("userId");
				if(record.getStr("signName")!=null){
					signNames+="，"+record.getStr("signName");
				}else{
					signNames+="，";
				}
				if(record.getStr("signName")!=null){
					signNum += record.getInt("signNum");
				}
				if(record.getStr("detail")==null){
					record.set("detail", "");
				}
			}
			signIds = signIds.substring(1);
			signNames = signNames.substring(1);
		}
		a.set("signIds", signIds);
		a.set("signNames", signNames);
		a.set("signNum", signNum+" ");
		return a ;
	}
}
