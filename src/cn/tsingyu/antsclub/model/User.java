package cn.tsingyu.antsclub.model;

import cn.tsingyu.antsclub.util.MD5Util;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class User extends Model<User>{
	public static final User dao = new User();
    // 用户登录
    public User login(String uname, String pwd){
        return dao.findFirst("select * from user where uname = ? and pwd = ? limit 1", uname, MD5Util.getMD5(pwd));
    }
    public boolean register(Object... paras){
    	User u = new User();
		u.set("uname", paras[0]);
		u.set("pwd", MD5Util.getMD5(paras[1].toString()));
		u.set("nickname", paras[2]);
		u.set("email", paras[3]);
		u.set("role", 1);
		return u.save();
    }
}
