package cn.tsingyu.antsclub.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class Account extends Model<Account>{
	public static final Account dao = new Account();
	public boolean createNewAccount(int uid,String uname){
		Account a = new Account();
		a.set("ownerId", uid);
		a.set("ownerName", uname);
		a.set("remark", "注册时自动创建");
		a.set("createUserId", uid);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		a.set("createTime", df.format(new Date()));
		return a.save();
	}
	public Account findByUserId(String uid){
		return dao.findFirst("select * from account where ownerId = ? ", uid);
	}
	public int updateBalance(String uid,Float amount){
		return Db.update("update account set balance=balance +? where ownerId = ?",amount,uid);
	}
}
