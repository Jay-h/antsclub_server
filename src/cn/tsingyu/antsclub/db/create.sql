CREATE DATABASE `antsclub_app` /*!40100 DEFAULT CHARACTER SET utf8 */;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uname` varchar(100) NOT NULL COMMENT '用户名',
  `pwd` varchar(100) NOT NULL COMMENT '密码',
  `email` varchar(100) DEFAULT NULL COMMENT '注册邮箱',
  `nickname` varchar(100) DEFAULT NULL COMMENT '昵称',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifyTime` datetime DEFAULT NULL COMMENT '最后修改时间',
  `role` int(1) NOT NULL COMMENT '角色',
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像，只存储middle大小的，large和small通过字符串替换获得',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uname` (`uname`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `nickname` (`nickname`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户表';

CREATE TABLE IF NOT EXISTS `activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `typeCode` varchar(36) DEFAULT NULL COMMENT '活动类型代码',
  `typeName` varchar(36) DEFAULT NULL COMMENT '活动类型名称',
  `payTypeCode` int(1) DEFAULT NULL COMMENT '付费类型：0，预付费；1，现场付费；2，AA付费；3，免费',
  `payTypeName` varchar(36) DEFAULT NULL COMMENT '付费类型名称',
  `payAmount` int(3) DEFAULT NULL COMMENT '付费金额',
  `title` varchar(100) NOT NULL COMMENT '活动名称',
  `startTime` datetime NOT NULL COMMENT '开始时间',
  `endTime` datetime NOT NULL COMMENT '结束时间',
  `address` varchar(100) NOT NULL COMMENT '活动地点',
  `detail` varchar(300) DEFAULT NULL COMMENT '活动详情',
  `maxNum` int(3) DEFAULT NULL COMMENT '限制人数',
  `minNum` int(3) DEFAULT NULL COMMENT '最少人数',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` varchar(36) DEFAULT NULL COMMENT '创建者',
  `createUserName` varchar(45) DEFAULT NULL COMMENT '创建者名称',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` varchar(36) DEFAULT NULL COMMENT '更新者',
  `updateUserName` varchar(45) DEFAULT NULL COMMENT '更新者名称',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='活动表';

CREATE TABLE IF NOT EXISTS `user_activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `activityId` int(11) NOT NULL COMMENT '活动ID',
  `userId` varchar(36) NOT NULL COMMENT '用户ID',
  `userName` varchar(100) DEFAULT NULL COMMENT '用户称呼',
  `signTime` datetime DEFAULT NULL COMMENT '报名时间',
  `signName` varchar(20) DEFAULT NULL COMMENT '报名称呼',
  `signNum` tinyint(3) unsigned DEFAULT NULL COMMENT '报名人数',
  `attendNum` int(3) DEFAULT NULL COMMENT '参加人数',
  `status` varchar(10) DEFAULT NULL COMMENT '活动状态',
  `isSign` varchar(1) DEFAULT NULL COMMENT '用于标识本条记录中的用户是否报名，1是0否',
  `isAttend` varchar(1) DEFAULT NULL COMMENT '用于标识本条记录中的用户是否参加活动，1是0否',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户活动表';

CREATE TABLE `account` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ownerId` int(10) DEFAULT NULL COMMENT '账户所有者id',
  `ownerName` varchar(20) DEFAULT NULL,
  `ownerTypeCode` int(1) DEFAULT '0' COMMENT '所有者类型：0，普通用户；1，群组用户',
  `accountTypeCode` int(1) DEFAULT '0' COMMENT '账户类型：0，金币（现金）；1，铜板（虚拟）',
  `balance` float DEFAULT '0' COMMENT '账户余额',
  `relateGroupId` int(10) DEFAULT '0' COMMENT '关联群组id',
  `remark` varchar(45) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` int(10) DEFAULT NULL COMMENT '创建者',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` int(10) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `account_unique` (`ownerId`,`accountTypeCode`,`relateGroupId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='账户表';

CREATE TABLE `trade` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `issue` varchar(100) DEFAULT NULL COMMENT '交易事项',
  `tradeType` int(1) DEFAULT NULL COMMENT '交易类型：0，财务充值和退款；1，退款; 2，消费',
  `tradeAmount` varchar(45) DEFAULT NULL COMMENT '交易金额',
  `sourceId` int(10) DEFAULT NULL COMMENT '源账户id',
  `sourceName` varchar(45) DEFAULT NULL COMMENT '源账户姓名',
  `targetId` int(10) DEFAULT NULL COMMENT '目标账户id',
  `targetName` varchar(45) DEFAULT NULL COMMENT '目标账户姓名',
  `activityId` int(11) DEFAULT NULL COMMENT '活动id，用于参加活动，扣费的时候，记录活动信息',
  `activityName` varchar(45) DEFAULT NULL COMMENT '活动名称，用于参加活动，扣费的时候，记录活动信息',
  `submitTime` datetime DEFAULT NULL COMMENT '交易他提交时间（用户向管理员付款的时间，管理员向用户退款的时间）',
  `payType` int(1) DEFAULT '0' COMMENT '交易方式：0，金币；1，铜板',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` int(10) DEFAULT NULL COMMENT '创建者',
  `createUserName` varchar(45) DEFAULT NULL COMMENT '创建者名称',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` int(10) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='账户日志表';