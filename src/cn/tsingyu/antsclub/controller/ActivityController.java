package cn.tsingyu.antsclub.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import cn.tsingyu.antsclub.common.Json;
import cn.tsingyu.antsclub.model.Account;
import cn.tsingyu.antsclub.model.Activity;
import cn.tsingyu.antsclub.model.Trade;
import cn.tsingyu.antsclub.model.UserActivity;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

public class ActivityController extends Controller {
	public void list(){
		Page<Activity> activityPage= Activity.dao.paginate(1,4);
		renderJson(activityPage.getList());
	}
	public void myList(){
		String uid = getPara("uid").trim();
		renderJson(Activity.dao.myList(1,4,uid).getList());
	}
	public void view(){
		Record r =  Activity.dao.getFunllInfo(getPara("actId").trim());
		renderJson(r);
	}
	public void saveOrUpdate(){
		Activity a =getModel(Activity.class);
		Json j = new Json();
		try{
			String uid = getPara("uid").trim();
			String uname = getPara("uname").trim();
			String typeCode = getPara("typeCode").trim();
			String typeName = getPara("typeName").trim();
			String title = getPara("title").trim();
			String address = getPara("address").trim();
			String startTime = getPara("startTime").trim();
			String endTime = getPara("endTime").trim();
			int maxNum = getParaToInt("maxNum");
			String payTypeCode = getPara("payTypeCode").trim();
			String payTypeName = getPara("payTypeName");
			String payAmount = getPara("payAmount").trim();
			String detail = getPara("detail");
			a.set("createUserId", uid);
			a.set("createUserName", uname);
			a.set("typeCode", typeCode);
			a.set("typeName", typeName);
			a.set("title", title);
			a.set("address", address);
			a.set("startTime", startTime);
			a.set("endTime", endTime);
			a.set("maxNum", maxNum);
			a.set("payTypeCode", payTypeCode);
			a.set("payTypeName", payTypeName);
			a.set("payAmount", payAmount);
			a.set("detail", detail);
			if(a.save()){
				j.setSuccess(true);
				j.setMsg("恭喜哈，活动发布成功了");
			}else{
				j.setSuccess(true);
				j.setMsg("糟糕，活动发布失败了");
			}
		}catch(Exception e){
			String exceptionMsg=e.getMessage();
			j.setSuccess(false);
			if(exceptionMsg==null){
				exceptionMsg = "糟糕，未知错误，请联系管理员";
			}
			e.printStackTrace();
			j.setMsg(exceptionMsg);
		}
		renderJson(j);
	}
	@Before(Tx.class)
	public void sign(){
		String actId = getPara("actId").trim();
		String uid = getPara("uid").trim();
		String uname = getPara("uname").trim();
		String signName = getPara("signName").trim();
		int signNum = getParaToInt("signNum");
		String type = getPara("type").trim();
		Json j = new Json();
		Record record = Activity.dao.getFunllInfo(actId);
		boolean isSign = UserActivity.dao.isSign(record,uid);
		if(type.equals("single")&&isSign){
			j.setSuccess(false);
			j.setMsg("您已经报过名了，不能重复报名，如果还有朋友要来，请点击带人报名");
		}else{
			j=UserActivity.dao.checkTime(record);
			if(j.getSuccess()){
				UserActivity originUa =null;
				int originSignNum = 0;
				if(type.equals("multiply")&&isSign&&record.getInt("payTypeCode")==0){
					originUa =  UserActivity.dao.isSign(actId, uid);
					originSignNum = originUa.getInt("signNum");
					int addSignNum =signNum - originSignNum;
					j=UserActivity.dao.checkBalance(record,uid,addSignNum);
				}else if(record.getInt("payTypeCode")==0){
					j=UserActivity.dao.checkBalance(record,uid,signNum);
				}
				if(j.getSuccess()){
					UserActivity ua = getModel(UserActivity.class);
					ua.set("activityId",actId);
					ua.set("userId",uid);
					ua.set("userName",uname);
					ua.set("signName",signName);
					ua.set("signNum",signNum);
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
					ua.set("signTime",df.format(new Date()));//new Date()为获取当前系统时间
					if(type.equals("single")||(type.equals("multiply")&&!isSign)){
						try{
							if(ua.save()){
								Float tradeAmount = -signNum*record.getFloat("payAmount");
								Account.dao.updateBalance(uid, tradeAmount);
								Trade t = new Trade();
								t.set("issue", "活动消费");
								t.set("tradeType", 2);
								t.set("tradeAmount", tradeAmount);
								t.set("sourceId", uid);
								t.set("sourceName", uname);
								t.set("activityId", actId);
								t.set("activityName", record.get("title"));
								t.set("remark", "活动报名时扣除");
								t.set("createTime",df.format(new Date()));
								t.set("createUserId", uid);
								t.set("createUserName", uname);
								t.save();
								j.setSuccess(true);
								j.setMsg("恭喜哈，报名成功了");
							}else{
								j.setSuccess(true);
								j.setMsg("糟糕，报名失败了");
							}
						}catch(Exception e){
							String exceptionMsg=e.getMessage();
							j.setSuccess(false);
							j.setMsg(exceptionMsg);
							e.printStackTrace();
						}
					}else{
						int uaId = originUa.getInt("id");
						ua.set("id", uaId);
						try{
							if(ua.update()){
								Float tradeAmount = -(signNum-originSignNum)*record.getFloat("payAmount");
								Account.dao.updateBalance(uid, tradeAmount);
								Trade t = new Trade();
								t.set("issue", "活动消费");
								t.set("tradeType", 2);
								t.set("tradeAmount", tradeAmount);
								t.set("sourceId", uid);
								t.set("sourceName", uname);
								t.set("activityId", actId);
								t.set("activityName", record.get("title"));
								t.set("remark", "更新报名");
								t.set("createTime",df.format(new Date()));
								t.set("createUserId", uid);
								t.set("createUserName", uname);
								t.save();
								j.setSuccess(true);
								j.setMsg("恭喜哈，更新报名成功了");
							}else{
								j.setSuccess(true);
								j.setMsg("糟糕，更新报名没成功");
							}
						}catch(Exception e){
							String exceptionMsg=e.getMessage();
							j.setSuccess(false);
							j.setMsg(exceptionMsg);
							e.printStackTrace();
						}
					}
				}
			}
		}
		renderJson(j);
	}
	@Before(Tx.class)
	public void undoSign(){
		String actId = getPara("actId").trim();
		String uid = getPara("uid").trim();
		String uname = getPara("uname").trim();
		String title = getPara("title").trim();
		Json j = new Json();
		UserActivity ua = UserActivity.dao.isSign(actId,uid);
		if(ua!=null){
			int signNum = ua.getInt("signNum");
			Activity a = Activity.dao.findById(actId);
			Float payAmount = a.getFloat("payAmount");
			if(UserActivity.dao.undoSign(actId, uid)>0){
				Float tradeAmount = signNum*payAmount;
				Account.dao.updateBalance(uid, tradeAmount);
				Trade t = new Trade();
				t.set("issue", "活动退款");
				t.set("tradeType", 2);
				t.set("tradeAmount", tradeAmount);
				t.set("sourceId", uid);
				t.set("sourceName", uname);
				t.set("activityId", actId);
				t.set("activityName", title);
				t.set("remark", "取消报名时退款");
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
				t.set("createTime",df.format(new Date()));
				t.set("createUserId", uid);
				t.set("createUserName", uname);
				t.save();
				j.setSuccess(true);
				j.setMsg("您已经取消报名");
			}else{
				j.setSuccess(false);
				j.setMsg("报名取消没成功");
			}
		}else{
			j.setSuccess(false);
			j.setMsg("您还没有报名了呢");
		}
		renderJson(j);
	}
	public void getSignInfo(){
		String actId = getPara("actId").trim();
		renderJson(Activity.dao.getSignInfo(actId));
	}
}
