package cn.tsingyu.antsclub.controller;

import cn.tsingyu.antsclub.model.Topic;

import com.jfinal.core.Controller;

public class TopicController extends Controller{
	public void index(){
		String cid = getPara("cid").trim();
		renderJson(Topic.dao.topicList(1,6,cid).getList());
	}
	public void view(){
		String id = getPara("id").trim();
		renderJson(Topic.dao.findById(id));
	}
}
