package cn.tsingyu.antsclub.common;

import cn.tsingyu.antsclub.controller.ActivityController;
import cn.tsingyu.antsclub.controller.IndexController;
import cn.tsingyu.antsclub.controller.TopicController;
import cn.tsingyu.antsclub.controller.UserController;
import cn.tsingyu.antsclub.model.Account;
import cn.tsingyu.antsclub.model.Activity;
import cn.tsingyu.antsclub.model.Topic;
import cn.tsingyu.antsclub.model.Trade;
import cn.tsingyu.antsclub.model.User;
import cn.tsingyu.antsclub.model.UserActivity;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;


/**
 * @Description: 
 * @author 土龙 heshilong2005@163.com
 * @date 2014-12-1 上午10:40:47
 */
public class Config extends JFinalConfig{

	@Override
	public void configConstant(Constants me) {
		// 加载少量配置，随后可用getProperty(...)取值
		loadPropertyFile("config.properties");
		me.setDevMode(getPropertyToBoolean("devMode", false));
	}

	@Override
	public void configRoute(Routes me) {
		me.add("/", IndexController.class,"/index");//第三个参数为该Controller的视图存放路径，默认与controller名字相同，在此即为"/index"
		me.add("/user",UserController.class);
		me.add("/activity",ActivityController.class);
		me.add("/topic",TopicController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		// 配置Druid数据库连接池插件
		DruidPlugin dp = new DruidPlugin(getProperty("jdbcUrl"),getProperty("user"),getProperty("password").trim());
		me.add(dp);
		
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
		me.add(arp);
		arp.addMapping("user", User.class); 
		arp.addMapping("activity", Activity.class);
		arp.addMapping("user_activity", UserActivity.class);
		arp.addMapping("account", Account.class);
		arp.addMapping("trade", Trade.class);
		arp.addMapping("topic", Topic.class);
	}

	@Override
	public void configInterceptor(Interceptors me) {
		
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler("base_path"));
	}
	
}
